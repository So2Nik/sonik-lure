name='dupeguru'
version='4.3.1'
release='1'
desc='Find duplicate files with various contents, using perceptual diff for pictures'
homepage='https://dupeguru.voltaicideas.net/'
maintainer="SoNik <so2nik@protonmail.com>"
architectures=('amd64')
license=('GPL3')
provides=('dupeguru')
conflicts=('dupeguru' 'dupeguru-git' 'dupeguru-bin')
deps=('python' 'python-pip' 'python-pyqt5' 'python-polib'
      'python-semantic-version' 'python-xxhash'
      'python-mutagen' 'python-send2trash' 'libxkbcommon-x11')
build_deps=('python-distro' 'python-sphinx')

sources=("https://github.com/arsenetar/dupeguru/archive/refs/tags/${name}-${version}.tar.gz")
checksums=('md5:996f2a9bab1541c188f823e9647f341c')

prepare() {
  cd "${srcdir}/${name}-${version}"
  sed -i -E 's/polib.*/polib>=1.1.0/g' requirements.txt
}

build() {
	cd "${srcdir}/${name}-${version}"
	# Instead of doing the full ./bootstrap.sh
  python3 -m venv env --system-site-packages
  source env/bin/activate
  python3 -m pip install -r requirements.txt
  msg "Starting build..."
  python build.py --clean
}

package() {
	cd "${srcdir}/${name}-${version}"
  
  cp -R "help" "build"
  cp -R "locale" "build"
  python package.py --fedora-pkg
  cd "build/${name}-unstable"

  mkdir -p "${pkgdir}/usr/share/applications"
  mv ${name}.desktop "${pkgdir}/usr/share/applications"
  
  mkdir -p "${pkgdir}/usr/share/${name}"
  cp -a -- * "${pkgdir}/usr/share/${name}/"
  chmod a+x "${pkgdir}/usr/share/${name}/run.py"
  
  mkdir -p "${pkgdir}/usr/share/pixmaps"
  ln -s "/usr/share/${name}/dgse_logo_128.png" "${pkgdir}/usr/share/pixmaps/${name}.png"
  mkdir -p "${pkgdir}/usr/bin"
  ln -s ../share/${name}/run.py "${pkgdir}/usr/bin/${name}"
}
